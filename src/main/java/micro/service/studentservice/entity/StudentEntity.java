package micro.service.studentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "student")
@Getter
@Setter
@NoArgsConstructor
public class StudentEntity extends BaseEntity {

  private String studentCode;
  private String firstName;
  private String lastName;
  private Date birthDay;
  private String classCode;
}
