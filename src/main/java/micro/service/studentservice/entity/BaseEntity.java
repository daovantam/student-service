package micro.service.studentservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@MappedSuperclass
@Getter
@Setter
public class BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID id;

  @CreationTimestamp
  @Column(name = "created_timestamp")
  private Timestamp createdTimestamp;

  @UpdateTimestamp
  @Column(name = "updated_timestamp")
  private Timestamp updatedTimestamp;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "updated_by")
  private String updatedBy;
}
