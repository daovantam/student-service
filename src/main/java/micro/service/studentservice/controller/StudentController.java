package micro.service.studentservice.controller;

import java.util.UUID;
import micro.service.studentservice.dto.SearchDto;
import micro.service.studentservice.dto.create.StudentDtoCreate;
import micro.service.studentservice.dto.update.StudentDtoUpdate;
import micro.service.studentservice.service.BaseService;
import micro.service.studentservice.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController implements BaseController<ResponseEntity, StudentDtoCreate, StudentDtoUpdate> {

  private StudentService studentService;

  @Autowired
  public StudentController(StudentService studentService) {
    this.studentService = studentService;
  }

  @Override
  @PostMapping
  public ResponseEntity create(StudentDtoCreate createDto) {
    studentService.create(createDto);
    return null;
  }

  @Override
  @PutMapping
  public ResponseEntity update(StudentDtoUpdate updateDto) {
    return null;
  }

  @Override
  @GetMapping
  public ResponseEntity get(UUID id) {
    return null;
  }

  @Override
  @DeleteMapping
  public ResponseEntity delete(UUID id) {
    return null;
  }

  @Override
  @PostMapping("/search")
  public ResponseEntity search(SearchDto searchDto) {
    return null;
  }

}
