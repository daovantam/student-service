package micro.service.studentservice.controller;

import java.util.UUID;
import micro.service.studentservice.dto.SearchDto;

public interface BaseController<R, C, U> {
  R create(C createDto);
  R update(U updateDto);
  R get(UUID id);
  R delete(UUID id);
  R search(SearchDto searchDto);
}
