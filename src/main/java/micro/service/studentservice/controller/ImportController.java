package micro.service.studentservice.controller;

import micro.service.studentservice.dto.create.StudentDtoCreate;
import micro.service.studentservice.dto.response.StudentDtoResponse;
import micro.service.studentservice.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/import")
public class ImportController {

    private ImportService importService;

    @Autowired
    public ImportController(ImportService importService) {
        this.importService = importService;
    }

    @PostMapping("/excel")
    public ResponseEntity<?> importStudent(@RequestBody List<StudentDtoCreate> studentDtoCreates) {
        List<StudentDtoResponse> studentDtoResponses = importService.importStudent(studentDtoCreates);
        return new ResponseEntity<>(studentDtoResponses, HttpStatus.OK);
    }
}
