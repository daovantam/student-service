package micro.service.studentservice.service;

import micro.service.studentservice.dto.create.StudentDtoCreate;
import micro.service.studentservice.dto.response.StudentDtoResponse;
import micro.service.studentservice.dto.update.StudentDtoUpdate;
import org.springframework.stereotype.Service;

@Service
public interface StudentService extends BaseService<StudentDtoResponse, StudentDtoCreate, StudentDtoUpdate> {

}
