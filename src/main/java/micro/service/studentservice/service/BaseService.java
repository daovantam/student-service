package micro.service.studentservice.service;

import java.util.UUID;

public interface BaseService <R, C, U> {
  R create(C createDto);
  R update(U updateDto);
  R get(UUID id);
  void delete(UUID id);
}