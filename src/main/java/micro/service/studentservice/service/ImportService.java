package micro.service.studentservice.service;

import micro.service.studentservice.dto.create.StudentDtoCreate;
import micro.service.studentservice.dto.response.StudentDtoResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ImportService {
    List<StudentDtoResponse> importStudent(List<StudentDtoCreate> studentDtoCreates);
}
