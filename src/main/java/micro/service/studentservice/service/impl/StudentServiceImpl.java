package micro.service.studentservice.service.impl;

import java.util.UUID;
import micro.service.studentservice.dto.create.StudentDtoCreate;
import micro.service.studentservice.dto.response.StudentDtoResponse;
import micro.service.studentservice.dto.update.StudentDtoUpdate;
import micro.service.studentservice.service.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {

  @Override
  public StudentDtoResponse create(StudentDtoCreate createDto) {
    return null;
  }

  @Override
  public StudentDtoResponse update(StudentDtoUpdate updateDto) {
    return null;
  }

  @Override
  public StudentDtoResponse get(UUID id) {
    return null;
  }

  @Override
  public void delete(UUID id) {

  }
}
