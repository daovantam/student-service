package micro.service.studentservice.service.impl;

import micro.service.studentservice.dto.create.StudentDtoCreate;
import micro.service.studentservice.dto.response.StudentDtoResponse;
import micro.service.studentservice.entity.StudentEntity;
import micro.service.studentservice.repository.StudentRepository;
import micro.service.studentservice.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImportServiceImpl implements ImportService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<StudentDtoResponse> importStudent(List<StudentDtoCreate> studentDtoCreates) {
        List<StudentEntity> studentEntities = new ArrayList<>();
        studentDtoCreates.forEach(student -> {
            StudentEntity studentEntity = new StudentEntity();
            studentEntity.setBirthDay(student.getBirthDay());
            studentEntity.setStudentCode(student.getStudentCode());
            studentEntity.setLastName(student.getLastName());
            studentEntity.setFirstName(student.getFirstName());
            studentEntity.setClassCode(student.getClassCode());
            studentEntities.add(studentEntity);
        });

        List<StudentEntity> response = studentRepository.saveAll(studentEntities);
        return null;
    }
}
