package micro.service.studentservice.dto.create;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class StudentDtoCreate {
    private String studentCode;
    private String firstName;
    private String lastName;
    private Date birthDay;
    private String classCode;
}
