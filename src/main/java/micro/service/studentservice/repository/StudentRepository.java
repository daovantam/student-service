package micro.service.studentservice.repository;

import java.util.UUID;
import micro.service.studentservice.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, UUID>,
    JpaSpecificationExecutor<StudentEntity> {

}
